function buildEditHref() {
  //"#{data.site.repo}blob/master/source/#{full_path}"

  const repo = document.querySelector('meta[property="og:repo"]').content
  const relativePath = document.querySelector('meta[property="og:relative_path"]').content

  return `${repo}blob/master/source/${relativePath}`
}

export default buildEditHref