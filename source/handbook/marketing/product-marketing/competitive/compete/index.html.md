---
layout: handbook-page-toc
title: "Competitive Resources"
noindex: true
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Competitors

| DevOps                                                        | SCM                                                  | CI-CD                                        | Security                                             |
|---------------------------------------------------------------|------------------------------------------------------|----------------------------------------------|------------------------------------------------------|
| ![GitHub][github-logo] [GitHub](#github)                      | ![Bitbucket][bitbucket-logo] [Bitbucket](#bitbucket) | ![Jenkins][jenkins-logo] [Jenkins](#jenkins) | ![Snyk][snyk-logo] [Snyk](#snyk)                     |
| ![Azure DevOps][azuredevops-logo] [AzureDevOps](#azuredevops) |                                                      |                                              | ![Synopsis][synopsis-logo] [Synopsis](#synopsis)     |
| ![Atlassian][atlassian-logo] [Atlassian](#atlassian)          |                                                      |                                              | ![Veracode][veracode-logo] [Veracode](#veracode)     |
| ![JFrog][jfrog-logo] [JFrog](#jfrog)                          |                                                      |                                              | ![CheckMarx][checkmarx-logo] [CheckMarx](#checkmarx) |

<!---
Table Source: https://docs.google.com/spreadsheets/d/1Ssdt-Uo7bwPfAxAYedxPP-jxhyfXJDE2mcCjkxN8-zc/edit#gid=0  
-->

-----

## GitHub<a name="github"></a>

|                         | **Sales Managers**                                                                                                                                                                                                                                                                                                       | **SAs**                                                                                                                                                                                                                                                                      |
|-------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Overview & Strategy** | [- SKO Presentation](https://docs.google.com/presentation/d/1WUdzaNn-tgm3Pa1W3JQyGjcR9JOR2a0rLUTA7zRNIt8/edit)                                                                                                                                                                                                           | [- SKO Presentation](https://docs.google.com/presentation/d/1WUdzaNn-tgm3Pa1W3JQyGjcR9JOR2a0rLUTA7zRNIt8/edit)                                                                                                                                                               |
| **Early Stage**         | - Post first meeting assets                                                                                                                                                                                                                                                                                              | - Demos                                                                                                                                                                                                                                                                      |
| **Evaluation Stage**    | [- GitLab Capabilities missing in GitHub](https://docs.google.com/presentation/d/1WAim4zkwaurifcaJn3bZp31CIzzf8AhnGTcbIdXpug8/edit#slide=id.g6fc8bb1d32_0_190) <br/> [- GitHub-GitLab comparison by license tiers](https://docs.google.com/presentation/d/1xVwIBrsrEIKjfdev9E4T7YaSQT_LVtH_kuex7wqAA_U/edit?usp=sharing) | [- GitHub-GitLab CI-CD Comparison](https://docs.google.com/presentation/d/1fDzXpgmvKyLpT0QTsGFnisFUeceNPsaL66vRVJ-sdpg/edit#slide=id.g29a70c6c35_0_68) <br/> [- GitHub Actions Gaps](https://about.gitlab.com/devops-tools/github-vs-gitlab.html#github-actions-gaps) <br/>  |
| **Closing Stage**       | - Objection Handling <br/> - ROI Analysis Examples <br/> [- GitLab-GitHub Support Comparison](https://docs.google.com/presentation/d/1-1kAayhvcyMhx3Sn_RxUqX1BGU1P4Y8s4Jf4TjKiwGs/edit#slide=id.g29a70c6c35_0_68)                                                                                                        |                                                                                                                                                                                                                                                                              |


----
## Azure DevOps<a name="azuredevops"></a>

----
## Atlassian<a name="atlassian"></a>

#### Jira<a name="jira"></a>
#### Bitbucket<a name="bitbucket"></a>
#### Bamboo<a name="bamboo"></a>

----
## JFrog<a name="jfrog"></a>

----
## Jenkins<a name="jenkins"></a>

----
## Snyk<a name="snyk"></a>

----
## Synopsis<a name="synopsis"></a>

----
## Veracode<a name="veracode"></a>

----
## Checkmarx<a name="checkmarx"></a>

----
[github-logo]: /images/devops-tools/github-logo-small.png "GitHub"
[azuredevops-logo]: /images/devops-tools/azure-devops-logo-small.png "Azure DevOps"
[jfrog-logo]: /images/devops-tools/jfrog-logo-small.png "JFrog"
[atlassian-logo]: /images/devops-tools/atlassian-logo-small.png "Atlassian"
[bitbucket-logo]: /images/devops-tools/bitbucket-logo-small.png "Bitbucket"
[jenkins-logo]: /images/devops-tools/jenkins-logo-small.png "Jenkins"
[snyk-logo]: /images/devops-tools/snyk-logo-small.png "Snyk"
[synopsis-logo]: /images/devops-tools/synopsis-logo-small.png "Synopsis"
[veracode-logo]: /images/devops-tools/veracode-logo-small.png "Veracode"
[checkmarx-logo]: /images/devops-tools/checkmarx-logo-small.png "CheckMarx"
