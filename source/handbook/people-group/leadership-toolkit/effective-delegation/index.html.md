---
layout: handbook-page-toc
title: "Effective Delegation"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Effective Delegation