---
layout: markdown_page
title: "ERG - MIT - Minorities in Tech"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Mission
We are the Minorities in Tech (MIT) Enterprise Resource Group (ERG)  founded in the fall of 2019. Along with GitLab Pride,  GitLab Women and GitLabDiverseABILITY, our goal is to ensure that members of these groups feel they have a safe space to communicate, unite and share issues that are reflective of their experience while at GitLab. This  group is not exclusionary and is open to anyone who is a GitLab employee

## Leads
* [Aricka Flowers](https://about.gitlab.com/company/team/#atflowers)
* [Sharif Bennett](https://about.gitlab.com/company/team/#SharifATL)

## Sub-committees

**Mentorship**: [Sharif Bennett](https://about.gitlab.com/company/team/#SharifATL) & [Darva Satcher](https://about.gitlab.com/company/team/#dsatcher)

**Events**: [Aricka Flowers](https://about.gitlab.com/company/team/#atflowers) & [Morgen Smith](https://about.gitlab.com/company/team/#msmith6)

**Recruiting**: [Lorna Webster](https://about.gitlab.com/company/team/#lwebster) & [Darva Satcher](https://about.gitlab.com/company/team/#dsatcher)

**Internal Outreach**:   [Romer Gonzalez](https://about.gitlab.com/company/team/#romerg) & [Israel Weeks](https://about.gitlab.com/company/team/#iweeks)

## Communications

* Slack Channel: #minoritiesintech
* Monthly Minorities in Tech Meeting

## Initiatives

The top four Initiatives for 2020 are:
* Mentorship
* Participation in external events
* Recruiting
* [Internal Outreach](https://about.gitlab.com/company/culture/inclusion/erg-minorities-in-tech/#internal-outreach)

## MIT Mentoring Program
Mentoring is important for employee retention and growth.  During the 2020 calendar year, MIT is hosting it's first Mentoring Program.  

[Learn More](https://about.gitlab.com/company/culture/inclusion/erg-minorities-in-tech/mentoring/)

### Internal Outreach

The purpose of the Internal Outreach Committee is to communicate information about the mission, events, and initiatives of the MIT group.

We do this by:

1. Working through Slack Channels to share information 
1. Organize roundtables at Contribute events
1. Coffee chats
1. Information provided in Onboarding issue


